<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
class HomeController extends Controller
{
    function __construct() {
        if (!session()->has('id')) {
            return redirect()->route('login');
        }
    }

    public function index() {

        $client = new Client();
        $response = $client->get('http://localhost/lumenapi/public/produto', [            
            'headers' => [   
                'Authorization' => 'Bearer ' . session()->get('token'),             
                'Content-Type' => 'application/json'
            ],
            'http_errors' => false,
        ]);

        $response = json_decode($response->getBody()->getContents());  
        
        if (isset($response->error)) {
            return redirect()->route('_home');
        }
        
        return view('home', [
            'produtos' => $response->produtos,
            'qtd' => count($response->produtos)
        ]);
    }
}
