<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class LoginController extends Controller
{
    public function index() {
        return view('login');
    }

    public function authenticate(Request $request) {  
        $validator = Validator::make($request->only('email', 'senha'), [
            'email'     => 'required|email',
            'senha'  => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->route('login');
        } 

        $email = $request->input('email');
        $senha = $request->input('senha');

        $client = new Client();
        $response = $client->post('http://localhost/lumenapi/public/auth/login', [
            'json' => [
                'email' => $email,
                'password' => $senha
            ],
            'headers' => [                
                'Content-Type' => 'application/json'
            ],
            'http_errors' => false,
        ]);

        $response = json_decode($response->getBody()->getContents());
        if(isset($response->data) && isset($response->token)) {

            $data = $response->data;
            session()->put([
                'id' => $data->id,
                'nome' => $data->nome,
                'email' => $data->email,
                'imagem' => $data->imagem,
                'token' => $response->token
            ]);

            $this->location();

            return redirect()->route('_home');            
        }
        return redirect()->route('login');
    }

    private function location() {
        $array = file_get_contents('https://api.myip.com'); // RECUPERA O ID REAL DO USUARIO
        $data = json_decode($array);
        $ip = $data->ip;
        
        $geolocation = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=$ip"));
        
        if ($geolocation) {
    
            $client = new Client();
            $response = $client->post('http://localhost/lumenapi/public/location', [
                'json' => [
                    'user_id' => session()->get('id'),
                    'ip' => $geolocation['geoplugin_request'],
                    'longitude' => $geolocation['geoplugin_longitude'],
                    'latitude' => $geolocation['geoplugin_latitude'],
                    'city' => $geolocation['geoplugin_city'],
                    'region_code' => $geolocation['geoplugin_regionCode'],
                    'region_name' => $geolocation['geoplugin_region'],
                    'country_code' => $geolocation['geoplugin_countryCode'],
                    'country_name' => $geolocation['geoplugin_countryName'] 
                ],
                'headers' => [                
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . session()->get('token')
                ],
                'http_errors' => false,
            ]);

            $response = json_decode($response->getBody()->getContents());            
        }

    }
}
