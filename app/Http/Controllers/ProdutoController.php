<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class ProdutoController extends Controller
{
    function __construct() {
        if (!session()->has('id')) {
            return redirect()->route('login');
        }
    }

    public function show($id) {
        if (!is_numeric($id)) {
            return redirect()->route('login');
        }

        $client = new Client();
        $response = $client->get("http://localhost/lumenapi/public/produto/$id", [            
            'headers' => [   
                'Authorization' => 'Bearer ' . session()->get('token'),             
                'Content-Type' => 'application/json'
            ],
            'http_errors' => false,
        ]);

        if ($response->getStatusCode() > 200) {
            return redirect()->route('_home')->with('error', 'ERRO AO ACESSAR O PRODUTO');
        }

        $response = json_decode($response->getBody()->getContents());

        $array = [];
        $json = array('Dia', 'Preço');
        array_push($array, $json);

        foreach($response->produtos as $value) {
            $arr = array(date('d/m', strtotime($value->created_at)), $value->preco);
            array_push($array, $arr);
        }

        return view('produto', [
            'produtos' => $response->produtos[0],
            'produto' => $response->produtos[0]->produto,
            'data_grafico' => json_encode($array, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES)
        ]);

    }

    public function store(Request $request) {
        $validator = Validator::make($request->only('link'), [
            'link' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->route('_home');
        } 

        $client = new Client();
        $response = $client->post('http://localhost/lumenapi/public/produto', [
            'json' => [
                'user_id' => session()->get('id'),
                'url' => $request->input('link')
            ],
            'headers' => [   
                'Authorization' => 'Bearer ' . session()->get('token'),             
                'Content-Type' => 'application/json'
            ],
            'http_errors' => false,
        ]);

        $response = json_decode($response->getBody()->getContents());
        if (isset($response->status) == 'success') {
            return redirect()->route('_home');
        } 

        return redirect()->route('_home')->with('error', 'LINK não pode ser Cadastrado');
    }

    public function delete(Request $request) {
        $validator = Validator::make($request->only('id'), [
            'id' => 'required|numeric'
        ]);

        $client = new Client();
        $response = $client->delete("http://localhost/lumenapi/public/produto/$request->id", [            
            'headers' => [   
                'Authorization' => 'Bearer ' . session()->get('token'),             
                'Content-Type' => 'application/json'
            ],
            'http_errors' => true,
        ]);

        $response = json_decode($response->getBody()->getContents());

        if (isset($response->status) == 'success') {
            return redirect()->route('_home');
        } 

        return redirect()->route('_home')->with('error', 'Produto não pode ser excluído');

    }
}
