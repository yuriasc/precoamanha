@extends('template.template')

@section('content')

<div class="container-fluid">

    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">                    
                    <div class="card">                        
                        <div class="header circle" style="background-image: url('{{ asset("perfil.png") }}');">
                        </div>  
                        <div class="body">
                            <center>
                                <h4>{{ session()->get('nome') }}</h4>
                                <h5>{{ session()->get('email') }}</h5>
                                <h5 class="bg-purple" style="padding: 5px;">
                                    @if($qtd == 0)
                                        {{ 'Nhenhum Produto' }}
                                    @elseif($qtd == 1)
                                        {{ '1 Produto' }}
                                    @else
                                        {{ $qtd . ' Produtos' }}
                                    @endif
                                </h5>                                
                            </center>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">                    
                    <div class="card" style="background: transparent; box-shadow: inherit; border: 0px;">
                        {{-- <center>
                            <img width="300" src="{{ asset('folks_2.jpg') }}">
                        </center> --}}
                    </div>
                </div>

            </div>

    </div>


    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">

        @if (session('error'))     
            <div id="alert-mensagem" class="alert alert-danger">                    
                <strong>Oops!</strong> {{ session('error') }}
            </div> 
        @endif

        
        <div class="card">                    
            <div class="body table-responsive">
                <form action="{{ route('_produtoStore') }}" method="POST">
                    @csrf
                    <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12" style="margin-bottom: 0px;">
                        <div class="form-group">
                            <div class="form-line">
                                <input type="text" name="link" class="form-control" placeholder="LINK DO PRODUTO">
                            </div>
                        </div>  
                    </div> 
                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">                                
                        <button type="submit" class="btn btn-block bg-purple waves-effect">CADASTRAR</button>
                    </div>                     
                </form>
            </div>
        </div>
    </div>

    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <div class="row clearfix">
                
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                PRODUTOS
                            </h2>
                        </div>
                        <div class="body">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>PRODUTO</th>
                                        <th>PREÇO</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($produtos as $key => $value)
                                        <tr data-href="{{ route('_produtoShow', [ 'id' => $value->url_id ]) }}" class="click" style="cursor:pointer">
                                            <td>{{ ++$key }}</td>
                                            <td>{{ $value->produto }}</td>
                                            <td>R$ {{ str_replace('.', ',', $value->preco) }}</td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="3"><center>Nenhum Resultado Encontrado</center></td>
                                        </tr>
                                    @endforelse
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

    </div>
</div>

<script>
    $('.click').on('click', function() {
        window.location = $(this).data("href");
    });
</script>

@endsection