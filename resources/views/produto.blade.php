@extends('template.template')

@section('content')

<div class="container-fluid">
    
    <div class="row clearfix">        
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card" style="width: 1500px">
                <div class="header">                    
                    <div class="row clearfix">
                        <div class="col-xs-12 col-sm-6">
                            <h2>{{ $produto }}</h2> 
                        </div>
                        <div class="col-xs-12 col-sm-6 align-right">
                            <div class="switch panel-switch-btn">
                                <form action="{{ route('_produtoDelete', [ 'id' => $produtos->url_id ]) }}" method="POST">
                                    @csrf
                                    <button type="submit" class="btnDelete btn bg-purple waves-effect">Excluir Produto</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="body">
                    <div id="chart_div" style="width: 100%; height: 500px;"></div>                    
                </div>
            </div>
        </div>
    </div>
    
</div>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
    var data = google.visualization.arrayToDataTable({!! $data_grafico !!});

    var options = {        
        hAxis: {title: 'Dia',  titleTextStyle: {color: '#333'}},
        vAxis: {minValue: 0}
    };

    var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
    chart.draw(data, options);
    }
</script>

@endsection