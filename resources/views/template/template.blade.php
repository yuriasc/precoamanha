﻿<!DOCTYPE html>
<html>

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">    
        <title>Preço.Amanhã</title>
        <!-- Favicon-->
        <link rel="icon" href="{{ asset('favicon.png') }}" type="image/x-icon">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

        <!-- Bootstrap Core Css -->
        <link href="{{ asset('template/plugins/bootstrap/css/bootstrap.css') }}" rel="stylesheet">

        <!-- Css Style -->
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">

        <!-- Waves Effect Css -->
        <link href="{{ asset('template/plugins/node-waves/waves.css') }}" rel="stylesheet" />

        <!-- Animation Css -->
        <link href="{{ asset('template/plugins/animate-css/animate.css') }}" rel="stylesheet" />

        <!-- Sweetalert Css -->
        <link href="{{ asset('template/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet" />

        <!-- Morris Css -->
        <link href="{{ asset('template/plugins/morrisjs/morris.css') }}" rel="stylesheet" />

        <!-- JQuery DataTable Css -->
        <link href="{{ asset('template/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">

        <!-- Custom Css -->
        <link href="{{ asset('template/css/style.css') }}" rel="stylesheet">

        <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
        <link href="{{ asset('template/css/themes/all-themes.css') }}" rel="stylesheet" />

        <!-- Jquery Core Js -->
        <script src="{{ asset('template/plugins/jquery/jquery.min.js') }}"></script>

    </head>

    <body class="theme-red">

        <nav class="navbar bg-purple">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                    <a href="javascript:void(0);" class="bars"></a>
                    <a class="navbar-brand" href="{{ route('_home') }}">Preço.Amanhã || {{ session()->get('nome') }}</a>
                </div>
            </div>
        </nav>
     
        <section class="content" style="margin: 100px 15px 0 20px;">
            
            @yield('content')

        </section>  

        <script>
            $(document).ready(function () {
                @if (session('error'))
                    $('#alert-mensagem').fadeOut(3000);
                @endif
            });
        </script>

        <!-- Bootstrap Core Js -->
        <script src="{{ asset('template/plugins/bootstrap/js/bootstrap.js') }}"></script>

        <!-- Slimscroll Plugin Js -->
        <script src="{{ asset('template/plugins/jquery-slimscroll/jquery.slimscroll.js') }}"></script>

        <!-- Waves Effect Plugin Js -->
        <script src="{{ asset('template/plugins/node-waves/waves.js') }}"></script>

        <!-- Jquery Validation Plugin Css -->
        <script src="{{ asset('template/plugins/jquery-validation/jquery.validate.js') }}"></script>

        <!-- Morris Plugin Js -->
        <script src="{{ asset('template/plugins/raphael/raphael.min.js') }}"></script>
        <script src="{{ asset('template/plugins/morrisjs/morris.js') }}"></script>

        <!-- Jquery DataTable Plugin Js -->
        <script src="{{ asset('template/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
        <script src="{{ asset('template/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>

        <!-- Custom Js -->
        <script src="{{ asset('template/js/admin.js') }}"></script>
        <script src="{{ asset('template/js/pages/tables/jquery-datatable.js') }}"></script>
        <script src="{{ asset('template/js/pages/forms/form-validation.js') }}"></script>

        <!-- Demo Js -->
        <script src="{{ asset('template/js/demo.js') }}"></script> 

        <!-- SweetAlert Plugin Js -->
        <script src="{{ asset('template/plugins/sweetalert/sweetalert.min.js') }}"></script>

        
        @yield('content_custom')

    </body>

</html>