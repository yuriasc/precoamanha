<?php

Route::post('/login/authenticate', 'LoginController@authenticate')->name('authenticate'); 
Route::get('/', 'LoginController@index')->name('login'); 


Route::get('/sistema/home', 'HomeController@index')->name('_home');
Route::post('/sistema/produto', 'ProdutoController@store')->name('_produtoStore');
Route::get('/sistema/produto/{id}', 'ProdutoController@show')->name('_produtoShow');
Route::post('/sistema/produto/{id}', 'ProdutoController@delete')->name('_produtoDelete');