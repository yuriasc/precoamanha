function limpa_formulário_cep() {
    $('input[name="logradouro"]').val("");
    $('input[name="bairro"]').val("");
}

$('input[name="cep"]').blur(function() {

    var cep = $(this).val().replace(/\D/g, '');
    if (cep != "") {

        var validacep = /^[0-9]{8}$/;
        if(validacep.test(cep)) {

            $('input[name="logradouro"]').val("...");
            $('input[name="bairro"]').val("...");
            
            $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                if (!("erro" in dados)) {
                    $('input[name="logradouro"]').focus();                    
                    $('input[name="logradouro"]').val(dados.logradouro);
                    $('input[name="bairro"]').focus();
                    $('input[name="bairro"]').val(dados.bairro);
                    $('input[name="numero"]').focus();
                } else {                    
                    limpa_formulário_cep();
                    alert("CEP não encontrado.");
                }
            });
        }
        else {            
            limpa_formulário_cep();
            alert("Formato de CEP inválido.");
        }
    } 
    else {        
        limpa_formulário_cep();
    }
});
